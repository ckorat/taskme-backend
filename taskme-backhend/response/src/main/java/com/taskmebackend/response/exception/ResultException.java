package com.taskmebackend.response.exception;

import com.taskmebackend.response.response.Result;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResultException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	Result<Object> resultExecption;
}
