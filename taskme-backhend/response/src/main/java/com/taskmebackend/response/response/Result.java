package com.taskmebackend.response.response;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Result<T> {
	private int code;
	private String message;
	private T data;
	private List<TaskMeError> errors;

	public Result() {
		super();
	}

	public Result(int code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public Result(int code, T data) {
		super();
		this.code = code;
		this.data = data;
	}

	public Result(int code, String message, T data) {
		super();
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public Result(int code, List<TaskMeError> error) {
		super();
		this.code = code;
		this.errors = error;
	}

	public Result(int code, String message, List<TaskMeError> error) {
		super();
		this.code = code;
		this.message = message;
		this.errors = error;
	}

	public Result(int code, TaskMeError error) {
		super();
		this.code = code;
		addErrorToList(error);
	}

	public Result(int code, String message, TaskMeError error) {
		super();
		this.code = code;
		this.message = message;
		addErrorToList(error);
	}

	public Result(int code, String message, T data, List<TaskMeError> error) {
		super();
		this.code = code;
		this.message = message;
		this.data = data;
		this.errors = error;
	}

	public void addErrorToList(TaskMeError error) {
		if (this.errors == null) {
			this.errors = new ArrayList<TaskMeError>();
		}
		this.errors.add(error);
	}

	@Data
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public static class TaskMeError {
		private int errorId;
		private String errorMessage;
		private String field;

		public TaskMeError() {
			super();
		}

		public TaskMeError(int errorId, String errorMessage) {
			super();
			this.errorId = errorId;
			this.errorMessage = errorMessage;
		}

		public TaskMeError(int errorId, String errorMessage, String field) {
			super();
			this.errorId = errorId;
			this.errorMessage = errorMessage;
			this.field = field;
		}
	}
}
