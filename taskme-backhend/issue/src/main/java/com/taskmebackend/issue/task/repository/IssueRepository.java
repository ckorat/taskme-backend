package com.taskmebackend.issue.task.repository;

import com.taskmebackend.issue.task.model.Issue;
import com.taskmebackend.issue.task.model.IssueDetails;
import com.taskmebackend.issue.task.model.IssueType;

import java.util.List;

public interface IssueRepository {
    List<Issue> getAllIssue();
    List<IssueType> getIssueType();
    List<IssueDetails> getIssueDetail(int id);
    List<IssueDetails> getIssueById(int id);
    List<Issue> getIssueByStatus(String Status);
    List<Issue> getIssueByAssigneeId(int id);
    boolean assignIssue(int id,int issue) throws  Exception;
    int AddIssue(Issue issue) throws  Exception;
    boolean updateIssueDetails(int id,Issue issue) throws Exception;
    boolean updateIssueStatus(int id,String Status) throws  Exception;
}
