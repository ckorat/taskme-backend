package com.taskmebackend.issue.sprint.repository;

import com.taskmebackend.issue.project.service.ProjectQueryService;
import com.taskmebackend.issue.sprint.model.Sprint;
import com.taskmebackend.issue.sprint.model.maper.SprintRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SprintRepositoryImpl implements SprintRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    ProjectQueryService projectQueryService;

    @Override
    public List<Sprint> findAllSprint() {
        return jdbcTemplate.query(projectQueryService.dbQueries.getFindAllSprint(),new SprintRowMapper());
    }

    @Override
    public List<Sprint> findActiveSprint(int projectid) {
        return jdbcTemplate.query(projectQueryService.dbQueries.findAllActiveSprint,new SprintRowMapper(),projectid);
    }

    @Override
    public List<Sprint> findInActiveSprint() {
        return jdbcTemplate.query(projectQueryService.dbQueries.findAllInActiveSprint,new SprintRowMapper());
    }

    @Override
    public List<Sprint> findSprintById(int id) {
        return jdbcTemplate.query(projectQueryService.dbQueries.findSprintById,new SprintRowMapper(),id);
    }

    @Override
    public int addSprint(Sprint sprint) throws Exception {
        return jdbcTemplate.update(projectQueryService.dbQueries.insertSprint,sprint.getProjectId(),sprint.getSprintName(),sprint.getSprintDescription(),sprint.getStartDate(),sprint.getEndDate(),sprint.getIsActive());
    }

    @Override
    public boolean updateSprint(int id, Sprint sprint) throws Exception {
        return jdbcTemplate.update(projectQueryService.dbQueries.updateSprint,sprint.getProjectId(),sprint.getSprintName(),sprint.getSprintDescription(),sprint.getStartDate(),sprint.getEndDate(),sprint.getIsActive(),id)>0;
    }
}
