package com.taskmebackend.issue.project.service;

import com.taskmebackend.issue.project.model.*;
import com.taskmebackend.issue.project.repository.ProjectRepository;
import com.taskmebackend.profile.model.DeveloperInvite;
import com.taskmebackend.response.exception.ResultException;
import com.taskmebackend.response.response.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ProjectService {
    @Autowired
    ProjectRepository projectRepository;

    public Result<List<Project>> findAllProject(){
        List<Project> list=projectRepository.findAllProject();
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new Result<>(404, "No Projects found"));
    }

    public Result<List<Project>> findAllActiveProject(int id){
        List<Project> list=projectRepository.getAllActiveProject(id);
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new Result<>(404, "No Project Actived Yet."));
    }

    public Result<List<Project>> findAllInactiveProject(int id){
        List<Project> list=projectRepository.getAllInactiveProject(id);
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new Result<>(404, "All Project Activeted"));
    }

    public Result<List<Project>> findProjectById(int id){
        List<Project> project=projectRepository.getProjectById(id);
        return  new Result<>(200,project);
    }

    public Result<Project> addProject(Project project) throws Exception{
        int id=projectRepository.addProject(project);
        if(id > 0 ){
            return  new Result<>(201,"Project Created");
        }
        throw new ResultException(new Result<>(400, "Error!, please try again!", new ArrayList<>(Arrays
                .asList(new Result.TaskMeError(400, "unable to add the given project")))));
    }

    public  Result<Project> updateProject(int id,Project project) throws  Exception{
        if(projectRepository.UpdateProjectById(id,project)){
            return  new Result<>(200,"Project Updated");
        }
        throw new ResultException(new Result<>(400, "Unable to update the given Project, please try again!",
                new ArrayList<>(Arrays.asList(new Result.TaskMeError(project.hashCode(),
                        "given ProjectId('" + id + "') does not exists ")))));
    }

    public  Result<UserProject> addUserProject(UserProject userProject) throws Exception{
        int id=projectRepository.addUserProject(userProject);
        if(id >0){
            return  new Result<>(201,"User Added in Project");
        }
        throw new ResultException(new Result<>(400, "Error!, please try again!", new ArrayList<>(Arrays
                .asList(new Result.TaskMeError(400, "unable to add user into the given project")))));

    }

    public  Result<UserProject> updateUserProject(int userid,String projectid) throws Exception{
        if(projectRepository.UpdateUserProjectById(userid,projectid)){
            return  new Result<>(200,"User Updated in Project");
        }
        throw new ResultException(new Result<>(400, "Error!, please try again!", new ArrayList<>(Arrays
                .asList(new Result.TaskMeError(400, "unable to add user into the given project")))));
    }

    public  Result<List<UserProject>> getProjectByUserId(int id){
        List<UserProject> user=projectRepository.getProjectByUserId(id);
        return new Result<>(200,user);
    }
    public Result<UserProject> removeUserProject(int userid) throws Exception {
        if (projectRepository.removeUserProject(userid)) {
            return new Result<>(200, "user project Deleted");
        }
        throw new ResultException(new Result<>(400, "Error!, please try again!", new ArrayList<>(Arrays
                .asList(new Result.TaskMeError(400, "unable to delete the given user project")))));
    }
    public  Result<List<ProjectList>> findPrjectListByUser(int id){
        List<ProjectList> list=projectRepository.getProjectListofUser(id);
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new Result<>(404, "No Projects found"));
    }

    public Result<List<MemberList>> getMemberList(int id){
        List<MemberList> list= projectRepository.getMemberList(id);
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new Result<>(404, "No Member found"));
    }

    public Result<List<MeetingId>> getMeetingId(){
        List<MeetingId> list=projectRepository.MeetingId();
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new Result<>(404, "No Meeting found"));
    }
}
