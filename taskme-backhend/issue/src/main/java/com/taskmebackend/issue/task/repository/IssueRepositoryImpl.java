package com.taskmebackend.issue.task.repository;
import com.taskmebackend.issue.project.service.ProjectQueryService;
import com.taskmebackend.issue.task.model.Issue;
import com.taskmebackend.issue.task.model.IssueDetails;
import com.taskmebackend.issue.task.model.IssueType;
import com.taskmebackend.issue.task.model.maper.IssueDetailsRowMapper;
import com.taskmebackend.issue.task.model.maper.IssueRowMapper;
import com.taskmebackend.issue.task.model.maper.IssueTypeRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class IssueRepositoryImpl implements IssueRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    ProjectQueryService projectQueryService;


    @Override
    public List<Issue> getAllIssue() {
        return jdbcTemplate.query(projectQueryService.dbQueries.allIssue,new IssueRowMapper());
    }

    @Override
    public List<IssueType> getIssueType() {
        return jdbcTemplate.query(projectQueryService.dbQueries.getIssueType,new IssueTypeRowMapper());
    }

    @Override
    public List<IssueDetails> getIssueDetail(int id) {
        return jdbcTemplate.query(projectQueryService.dbQueries.getIssueDetail,new IssueDetailsRowMapper(),id);
    }

    @Override
    public List<IssueDetails> getIssueById(int id) {
        return jdbcTemplate.query(projectQueryService.dbQueries.getIssueById,new IssueDetailsRowMapper(),id);
    }

    @Override
    public List<Issue> getIssueByStatus(String Status) {
        return jdbcTemplate.query(projectQueryService.dbQueries.getIssueByStatus,new IssueRowMapper(),Status);
    }

    @Override
    public List<Issue> getIssueByAssigneeId(int id) {
        return jdbcTemplate.query(projectQueryService.dbQueries.getIssueByAssigneeId,new IssueRowMapper(),id);
    }

    @Override
    public boolean assignIssue(int id, int issue) throws Exception {
        return jdbcTemplate.update(projectQueryService.dbQueries.assignIssue,issue,id) > 0;
    }

    @Override
    public int AddIssue(Issue issue) throws Exception {
        return jdbcTemplate.update(projectQueryService.dbQueries.getAddIssue(),issue.getSprintid(),issue.getIssueName(),issue.getIssueDescription(),issue.getCreator(),issue.getAssignee(),issue.getCreateOn(),issue.getUpdateOn(),issue.getStoryPoint(),issue.getIssueTypeId(),issue.getIssueStatus());
    }

    @Override
    public boolean updateIssueDetails(int id, Issue issue) throws Exception {
        return jdbcTemplate.update(projectQueryService.dbQueries.updateIssueDetails,issue.getSprintid(),issue.getIssueName(),issue.getIssueDescription(),issue.getCreator(),issue.getAssignee(),issue.getCreateOn(),issue.getUpdateOn(),issue.getStoryPoint(),issue.getIssueTypeId(),issue.getIssueStatus(),id) > 0;
    }

    @Override
    public boolean updateIssueStatus(int id, String Status) throws Exception {
        return jdbcTemplate.update(projectQueryService.dbQueries.updateIssueStatus,Status,id) > 0;
    }
}
