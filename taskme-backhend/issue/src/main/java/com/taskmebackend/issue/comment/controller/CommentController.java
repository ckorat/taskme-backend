package com.taskmebackend.issue.comment.controller;

import com.taskmebackend.issue.comment.model.Comment;
import com.taskmebackend.issue.comment.model.CommentDetail;
import com.taskmebackend.issue.comment.service.CommentService;
import com.taskmebackend.issue.project.model.Project;
import com.taskmebackend.response.response.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/comments")
public class CommentController {
    @Autowired
    CommentService commentService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<Result<List<CommentDetail>>> findCommentByIssueId(@PathVariable(value = "id") int id) throws Exception {
        Result<List<CommentDetail>> result = commentService.findCommentByIssueId(id);
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }
    @PostMapping(value = "/")
    public ResponseEntity<Result<Comment>> addComment(@RequestBody @Valid Comment comment) throws Exception {
        Result<Comment> result= commentService.addComment(comment);
        return  new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }

}
