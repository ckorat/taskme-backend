package com.taskmebackend.issue.project.controller;

import com.taskmebackend.issue.project.model.*;
import com.taskmebackend.issue.project.service.ProjectService;
import com.taskmebackend.profile.model.DeveloperInvite;
import com.taskmebackend.response.response.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/projects")

public class ProjectController {

    @Autowired
    ProjectService projectService;

    @GetMapping(value = "/")
    public ResponseEntity<Result<List<Project>>> getAllProject(){
        Result<List<Project>> result=projectService.findAllProject();
        return  new ResponseEntity<>(result,  HttpStatus.valueOf(result.getCode()));
    }

    @GetMapping(value = "/active/{userid}")
    public ResponseEntity<Result<List<Project>>> getAllActiveProject(@PathVariable("userid") int userid){
        Result<List<Project>> result =projectService.findAllActiveProject(userid);
        return  new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }

    @GetMapping(value = "/inactive/{userid}")
    public  ResponseEntity<Result<List<Project>>> getAllInActiveProject(@PathVariable("userid") int userid){
        Result<List<Project>> result=projectService.findAllInactiveProject(userid);
        return new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Result<List<Project>>> findProjectById(@PathVariable("id") int id){
        Result<List<Project>> result=projectService.findProjectById(id);
        return new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }

    @GetMapping(value = "list/{id}")
    public  ResponseEntity<Result<List<ProjectList>>> findProjectListbyUser(@PathVariable("id") int id){
        Result<List<ProjectList>> result=projectService.findPrjectListByUser(id);
        return  new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }

    @GetMapping(value = "member/{id}")
    public  ResponseEntity<Result<List<MemberList>>> findMember(@PathVariable("id") int id){
        Result<List<MemberList>> result=projectService.getMemberList(id);
        return  new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }
    @PostMapping(value = "/")
    public ResponseEntity<Result<Project>> addProject(@RequestBody(required = true) Project project) throws Exception{
        Result<Project> result=projectService.addProject(project);
        return  new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }

    @PutMapping(value = "/")
    public ResponseEntity<Result<Project>> updateProject(@RequestBody(required = true) Project project) throws Exception{
        Result<Project> result=projectService.updateProject(project.getId(),project);
        return new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }

    @GetMapping("/user/{userid}")
    public ResponseEntity<Result<List<UserProject>>> getProjectByUserId(@PathVariable("userid") int id){
        Result<List<UserProject>> result=projectService.getProjectByUserId(id);
        return  new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }

    @PostMapping(value = "/user")
    public ResponseEntity<Result<UserProject>> addUserProject(@RequestBody(required = true) UserProject userProject) throws Exception{
        Result<UserProject> result=projectService.addUserProject(userProject);
        return new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }

    @PutMapping(value = "/user/")
    public ResponseEntity<Result<UserProject>> updateUserProject(@RequestBody(required = true) UserProject userProject) throws Exception{
        Result<UserProject> result=projectService.updateUserProject(userProject.getUserId(),userProject.getProjectId());
        return new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }
    @DeleteMapping(value = "/user/{userid}")
    public ResponseEntity<Result<UserProject>>  removeUserProject( @PathVariable("userid") int userid) throws Exception{
        Result<UserProject> result = projectService.removeUserProject(userid);
        return  new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }


    @GetMapping(value ="/meetingid")
    public ResponseEntity<Result<List<MeetingId>>> meetingId(){
        Result<List<MeetingId>> result=projectService.getMeetingId();
        return new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }
}
