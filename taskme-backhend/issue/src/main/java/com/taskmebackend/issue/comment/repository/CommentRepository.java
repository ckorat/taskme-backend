package com.taskmebackend.issue.comment.repository;

import com.taskmebackend.issue.comment.model.Comment;
import com.taskmebackend.issue.comment.model.CommentDetail;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository {
    boolean addComment(Comment comment) throws  Exception;
    List<CommentDetail> findCommentByIssueId(int id) throws Exception;
}
