package com.taskmebackend.issue.task.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Issue {
    private int id;
    private int sprintid;
    private String issueName;
    private String issueDescription;
    private int creator;
    private String assignee;
    private String createOn;
    private String updateOn;
    private int storyPoint;
    private int issueTypeId;
    private String issueStatus;
}
