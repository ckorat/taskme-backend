package com.taskmebackend.issue.sprint.repository;

import com.taskmebackend.issue.sprint.model.Sprint;

import java.util.List;

public interface SprintRepository {
    List<Sprint> findAllSprint();
    List<Sprint> findActiveSprint(int projectid);
    List<Sprint> findInActiveSprint();
    List<Sprint> findSprintById(int id);
    int addSprint(Sprint sprint) throws Exception;
    boolean updateSprint(int id,Sprint sprint) throws Exception;
}
