package com.taskmebackend.issue.sprint.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Sprint {
    private int id;
    private int projectId;
    private String sprintName;
    private String sprintDescription;
    private String startDate;
    private String endDate;
    private int isActive;

}
