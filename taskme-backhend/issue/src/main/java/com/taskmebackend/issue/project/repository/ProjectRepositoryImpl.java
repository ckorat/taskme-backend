package com.taskmebackend.issue.project.repository;

import com.taskmebackend.issue.project.model.*;
import com.taskmebackend.issue.project.model.maper.*;
import com.taskmebackend.issue.project.service.ProjectQueryService;
import com.taskmebackend.issue.project.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class ProjectRepositoryImpl implements ProjectRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    ProjectQueryService projectQueryService;


    @Override
    public List<Project> findAllProject() {
        return jdbcTemplate.query(projectQueryService.dbQueries.findAllProject,new ProjectRowMapper());
    }

    @Override
    public List<Project> getAllActiveProject(int id) {
        return jdbcTemplate.query(projectQueryService.dbQueries.getFindAllActiveProject(),new ProjectRowMapper(),id);
    }

    @Override
    public List<Project> getAllInactiveProject(int id) {
        return jdbcTemplate.query(projectQueryService.dbQueries.getFindAllInActiveProject(),new ProjectRowMapper(),id);
    }

    @Override
    public List<Project> getProjectById(int id) {
        return jdbcTemplate.query(projectQueryService.dbQueries.getFindProjectById(),new ProjectRowMapper(),id);
    }

    @Override
    public List<ProjectList> getProjectListofUser(int id) {
        return  jdbcTemplate.query(projectQueryService.dbQueries.getProjectListofUser,new ProjectListRowMapper(),id);
    }

    @Override
    public List<MemberList> getMemberList(int id) {
        return jdbcTemplate.query(projectQueryService.dbQueries.getGetMemberList(),new MemberListRowMapper(),id);
    }

    @Override
    public int addProject(Project project) throws Exception {
        return jdbcTemplate.update(projectQueryService.dbQueries.getInsertProject(),project.getOrgId(),project.getProjectName(),project.getProjectDescription(),project.getCreateOn(),project.getCompleteOn());
    }

    @Override
    public List<MeetingId> MeetingId() {
        return jdbcTemplate.query(projectQueryService.dbQueries.getMeetingId(),new MeetingIdRowMapper());
    }

    @Override
    public boolean UpdateProjectById(int id, Project project) throws Exception {
        return jdbcTemplate.update(projectQueryService.dbQueries.getUpdateProject(),project.getOrgId(),project.getProjectName(),project.getProjectDescription(),project.getCreateOn(),project.getCompleteOn(),id) > 0;
    }

    @Override
    public int addUserProject(UserProject userProject) throws Exception {
        return jdbcTemplate.update(projectQueryService.dbQueries.getAddUserProject(),userProject.getUserId(),userProject.getProjectId());
    }

    @Override
    public boolean UpdateUserProjectById(int userid, String projectid) throws Exception {
        return jdbcTemplate.update(projectQueryService.dbQueries.getUpdateUserProject(),projectid,userid)>0;
    }

    @Override
    public List<UserProject> getProjectByUserId(int userid) {
        return jdbcTemplate.query(projectQueryService.dbQueries.getProjectByUserId,new UserProjectRowMapper(),userid);
    }

    @Override
    public boolean removeUserProject(int userid) throws Exception {
        return jdbcTemplate.update(projectQueryService.dbQueries.getRemoveUserProject(),userid)>0;
    }
}
