package com.taskmebackend.issue.sprint.model.maper;

import com.taskmebackend.issue.project.model.Project;
import com.taskmebackend.issue.sprint.model.Sprint;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SprintRowMapper implements RowMapper<Sprint> {
    @Override
    public Sprint mapRow(ResultSet resultSet, int i) throws SQLException {
        Sprint sprint=new Sprint();
        sprint.setId(resultSet.getInt("id"));
        sprint.setProjectId(resultSet.getInt("projectid"));
        sprint.setSprintName(resultSet.getString("sprintname"));
        sprint.setSprintDescription(resultSet.getString("sprintdescription"));
        sprint.setStartDate(resultSet.getString("startdate"));
        sprint.setEndDate(resultSet.getString("enddate"));
        sprint.setIsActive(resultSet.getInt("isactive"));
        return sprint;
    }
}
