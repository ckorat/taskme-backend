package com.taskmebackend.issue.comment.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Comment{
    private int id;
    private int issueid;
    private int commenter;
    private String comment;
    private String commentdatetime;
}
