package com.taskmebackend.issue.project.model.maper;
import com.taskmebackend.issue.project.model.Project;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProjectRowMapper implements RowMapper<Project> {

    @Override
    public Project mapRow(ResultSet resultSet, int i) throws SQLException {
       Project project = new Project();
       project.setId(resultSet.getInt("id"));
       project.setOrgId(resultSet.getInt("orgid"));
       project.setProjectName(resultSet.getString("projectname"));
       project.setProjectDescription(resultSet.getString("projectdescription"));
       project.setCreateOn(resultSet.getString("CreateOn"));
       project.setCompleteOn(resultSet.getString("CompleteOn"));
       return project;
    }
}
