package com.taskmebackend.issue.task.model.maper;

import com.taskmebackend.issue.task.model.Issue;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class IssueRowMapper implements RowMapper<Issue> {
    @Override
    public Issue mapRow(ResultSet resultSet, int i) throws SQLException {
        Issue issue=new Issue();
        issue.setId(resultSet.getInt("id"));
        issue.setSprintid(resultSet.getInt("sprintid"));
        issue.setIssueName(resultSet.getString("issuename"));
        issue.setIssueDescription(resultSet.getString("issuedescription"));
        issue.setCreator(resultSet.getInt("creator"));
        issue.setAssignee(resultSet.getString("assignee"));
        issue.setCreateOn(resultSet.getString("createon"));
        issue.setUpdateOn(resultSet.getString("updateon"));
        issue.setStoryPoint(resultSet.getInt("storypoint"));
        issue.setIssueTypeId(resultSet.getInt("issuetypeid"));
        issue.setIssueStatus(resultSet.getString("issuestatus"));
        return issue;
    }
}
