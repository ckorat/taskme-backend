package com.taskmebackend.issue.comment.repository;


import com.taskmebackend.issue.comment.model.Comment;
import com.taskmebackend.issue.comment.model.CommentDetail;
import com.taskmebackend.issue.comment.model.mapper.CommentRowMapper;
import com.taskmebackend.issue.comment.service.CommentQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(value = "commentRepository")
public class CommentRepositoryImpl implements CommentRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    CommentQueryService commentQueryService;

    @Override
    public boolean addComment(Comment comment) throws Exception {
        return jdbcTemplate.update(commentQueryService.getDbQueries().getInsertComment(),comment.getIssueid(),comment.getCommenter(),comment.getComment(),comment.getCommentdatetime())>0;
    }

    @Override
    public List<CommentDetail> findCommentByIssueId(int id) throws Exception {
        return jdbcTemplate.query(commentQueryService.getDbQueries().getFindCommentByIssueId(),new CommentRowMapper(),id);
    }
}

