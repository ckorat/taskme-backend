package com.taskmebackend.issue.sprint.controller;

import com.taskmebackend.issue.sprint.model.Sprint;
import com.taskmebackend.issue.sprint.service.SprintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.taskmebackend.response.response.Result;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/sprints")
public class SprintController {
    @Autowired
    SprintService sprintService;

    @GetMapping(value = "/")
    public ResponseEntity<Result<List<Sprint>>> findAllSprint(){
        Result<List<Sprint>> list=sprintService.findAllSprint();
        return  new ResponseEntity<>(list, HttpStatus.valueOf(list.getCode()));
    }

    @GetMapping(value = "/active/{projectid}")
    public ResponseEntity<Result<List<Sprint>>> findAllActiveSprint(@PathVariable("projectid")int projectid){
        Result<List<Sprint>> result=sprintService.findActiveSprint(projectid);
        return  new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @GetMapping(value = "/inactive")
    public ResponseEntity<Result<List<Sprint>>> findInActiveSprint(){
        Result<List<Sprint>> result=sprintService.findInActiveSprint();
        return  new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Result<List<Sprint>>> findSprintById(@PathVariable("id") int id){
        Result<List<Sprint>> result=sprintService.findSprintById(id);
        return  new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @PostMapping(value = "/")
    public ResponseEntity<Result<List<Sprint>>> addSprint(@RequestBody(required = true) Sprint sprint) throws Exception{
        Result<List<Sprint>> result=sprintService.addSprint(sprint);
        return  new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @PutMapping(value = "/")
    public ResponseEntity<Result<List<Sprint>>> updateSprint(@RequestBody(required = true) Sprint sprint) throws Exception{
        Result<List<Sprint>> result=sprintService.updateSprint(sprint.getId(),sprint);
        return  new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }
}
