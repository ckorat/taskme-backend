package com.taskmebackend.issue.comment.service;

import com.taskmebackend.issue.comment.model.Comment;
import com.taskmebackend.issue.comment.model.CommentDetail;
import com.taskmebackend.issue.comment.repository.CommentRepository;
import com.taskmebackend.issue.project.model.Project;
import com.taskmebackend.response.exception.ResultException;
import com.taskmebackend.response.response.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class CommentService {
    @Autowired
    @Qualifier("commentRepository")
    CommentRepository commentRepository;

    public Result<List<CommentDetail>> findCommentByIssueId(int id) throws Exception {
        List<CommentDetail> list = commentRepository.findCommentByIssueId(id);
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new Result<>(404, "No comment found"));
    }
    public Result<Comment> addComment(Comment comment) throws Exception{
        if(commentRepository.addComment(comment)){
            return  new Result<>(201,"Comment Added");
        }
        throw new ResultException(new Result<>(400, "Error!, please try again!", new ArrayList<>(Arrays
                .asList(new Result.TaskMeError(400, "unable to add the given comment")))));
    }
}