package com.taskmebackend.issue.task.model.maper;

import com.taskmebackend.issue.task.model.IssueDetails;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class IssueDetailsRowMapper implements RowMapper<IssueDetails> {
    @Override
    public IssueDetails mapRow(ResultSet resultSet, int i) throws SQLException {
        IssueDetails details=new IssueDetails();
        details.setId(resultSet.getInt("id"));
        details.setSprintid(resultSet.getInt("sprintid"));
        details.setIssueName(resultSet.getString("issuename"));
        details.setIssueDescription(resultSet.getString("issuedescription"));
        details.setAssignee(resultSet.getString("assignee"));
        details.setCreateOn(resultSet.getString("createon"));
        details.setUpdateOn(resultSet.getString("updateon"));
        details.setStoryPoint(resultSet.getInt("storypoint"));
        details.setIssueTypeId(resultSet.getInt("issuetypeid"));
        details.setIssueStatus(resultSet.getString("issuestatus"));
        details.setUsername(resultSet.getString("username"));
        details.setIssuetypename(resultSet.getString("issuetypename"));
        return details;
    }
}
