package com.taskmebackend.issue.task.service;

import com.sun.org.apache.bcel.internal.generic.ISUB;
import com.taskmebackend.issue.task.model.Issue;
import com.taskmebackend.issue.task.model.IssueDetails;
import com.taskmebackend.issue.task.model.IssueType;
import com.taskmebackend.issue.task.repository.IssueRepository;
import com.taskmebackend.response.exception.ResultException;
import com.taskmebackend.response.response.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class IssueService {
    @Autowired
    IssueRepository issueRepository;

    public Result<List<Issue>> findAllIssue(){
        List<Issue> list=issueRepository.getAllIssue();
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new com.taskmebackend.response.response.Result<>(404, "No Issues found"));
    }
    public Result<List<IssueType>> findIssueType(){
        List<IssueType> list=issueRepository.getIssueType();
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new com.taskmebackend.response.response.Result<>(404, "No Issues type found"));
    }

    public Result<List<IssueDetails>> findIssueById(int id){
        List<IssueDetails> list=issueRepository.getIssueById(id);
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new com.taskmebackend.response.response.Result<>(404, "No Issue found"));
    }
    public Result<List<IssueDetails>> findIssueSprintDetail(int id){
        List<IssueDetails> list=issueRepository.getIssueDetail(id);
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new com.taskmebackend.response.response.Result<>(404, "No Issue found"));
    }

    public  Result<List<Issue>> findIssueByStatus(String Status){
        List<Issue> list=issueRepository.getIssueByStatus(Status);
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new com.taskmebackend.response.response.Result<>(404, "No Issue found"));
    }

    public Result<List<Issue>> findIssueByAssigneeId(int id){
        List<Issue> list=issueRepository.getIssueByAssigneeId(id);
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new com.taskmebackend.response.response.Result<>(404, "No Issue found"));
    }

    public Result<List<Issue>> assignIssue(int id,int issue) throws Exception{
        if(issueRepository.assignIssue(id,issue)){
            return new Result<>(201,"Issue Assign to Developer");
        }
        throw new ResultException(new com.taskmebackend.response.response.Result<>(404, "No Issue found Under"+id+"this ID"));
    }

    public Result<List<Issue>> addIssue(Issue issue) throws Exception{
        int id=issueRepository.AddIssue(issue);
        if(id > 0){
            return new Result<>(201,"Issue Added to Sprint");
        }
        throw new ResultException(new Result<>(400, "Error!, please try again!", new ArrayList<>(Arrays
                .asList(new Result.TaskMeError(400, "unable to add the given Issue in Sprint")))));
    }

    public Result<List<Issue>> updateIssueDetails(int id,Issue issue) throws Exception{
        if(issueRepository.updateIssueDetails(id,issue)){
            return new Result<>(200,"Issue Updated");
        }
        throw new ResultException(new Result<>(400, "Unable to update the given Sprint, please try again!",
                new ArrayList<>(Arrays.asList(new Result.TaskMeError(issue.hashCode(),
                        "given SprintId('" + id + "') does not exists ")))));
    }

    public Result<List<Issue>> updateIssueStatus(int id,String Status) throws Exception{
        if(issueRepository.updateIssueStatus(id,Status)){
            return new Result<>(200,"Issue Status Has been changed");
        }
        throw new ResultException(new com.taskmebackend.response.response.Result<>(404, "No Issue found Under"+id+"this ID"));

    }


}
