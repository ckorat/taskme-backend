package com.taskmebackend.issue.project.model.maper;

import com.taskmebackend.issue.project.model.MeetingId;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MeetingIdRowMapper implements RowMapper<MeetingId> {
    @Override
    public MeetingId mapRow(ResultSet resultSet, int i) throws SQLException {
        MeetingId meetingId=new MeetingId();
        meetingId.setId(resultSet.getInt("id"));
        return meetingId;
    }
}
