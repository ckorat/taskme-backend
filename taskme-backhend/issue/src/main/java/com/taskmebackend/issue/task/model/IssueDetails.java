package com.taskmebackend.issue.task.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IssueDetails extends Issue {
    private String username;
    private String issuetypename;
}
