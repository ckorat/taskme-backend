package com.taskmebackend.issue.task.controller;
import com.taskmebackend.issue.task.model.Issue;
import com.taskmebackend.issue.task.model.IssueDetails;
import com.taskmebackend.issue.task.model.IssueType;
import com.taskmebackend.issue.task.service.IssueService;
import com.taskmebackend.response.response.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
@RestController
@RequestMapping(value = "/issues")
public class IssueController {
    @Autowired
    IssueService issueService;

    @GetMapping(value = "/")
    public ResponseEntity<Result<List<Issue>>> findAllIssue(){
        Result<List<Issue>> list=issueService.findAllIssue();
        return  new ResponseEntity<>(list, HttpStatus.valueOf(list.getCode()));
    }
    @GetMapping(value = "/type")
    public ResponseEntity<Result<List<IssueType>>> findIssueType(){
        Result<List<IssueType>> list=issueService.findIssueType();
        return  new ResponseEntity<>(list, HttpStatus.valueOf(list.getCode()));
    }
    @GetMapping(value = "/sprints/{sprintId}/issues/")
    public ResponseEntity<Result<List<IssueDetails>>> findIssueSprintDetail(@PathVariable("sprintId") int sprintId){
        Result<List<IssueDetails>> list=issueService.findIssueSprintDetail(sprintId);
        return  new ResponseEntity<>(list, HttpStatus.valueOf(list.getCode()));
    }

    @GetMapping(value = "/issue/{id}")
    public ResponseEntity<Result<List<IssueDetails>>> findIssueById(@PathVariable("id") int id){
        Result<List<IssueDetails>> list=issueService.findIssueById(id);
        return  new ResponseEntity<>(list, HttpStatus.valueOf(list.getCode()));
    }

    @GetMapping(value = "/{status}")
    public  ResponseEntity<Result<List<Issue>>> findIssueByStatus(@PathVariable("status") String status){
        Result<List<Issue>> list=issueService.findIssueByStatus(status);
        return  new ResponseEntity<>(list, HttpStatus.valueOf(list.getCode()));
    }

    @GetMapping(value = "/assignee/{id}")
    public  ResponseEntity<Result<List<Issue>>> findIssueByAssigneeId(@PathVariable("id") int id){
        Result<List<Issue>> list=issueService.findIssueByAssigneeId(id);
        return  new ResponseEntity<>(list, HttpStatus.valueOf(list.getCode()));
    }

    @PutMapping(value = "assign/{id}/{issue}")
    public  ResponseEntity<Result<List<Issue>>> assignIssue(@PathVariable("id") int id,@PathVariable("issue") int issue) throws Exception{
        Result<List<Issue>> list=issueService.assignIssue(id,issue);
        return  new ResponseEntity<>(list, HttpStatus.valueOf(list.getCode()));
    }

    @PostMapping(value = "/")
    public ResponseEntity<Result<List<Issue>>> addIssue(@RequestBody(required = true) Issue issue) throws  Exception{
        Result<List<Issue>> list=issueService.addIssue(issue);
        return  new ResponseEntity<>(list, HttpStatus.valueOf(list.getCode()));
    }

    @PutMapping(value = "/")
    public ResponseEntity<Result<List<Issue>>> updateIssue(@RequestBody(required = true) Issue issue) throws  Exception{
        Result<List<Issue>> list=issueService.updateIssueDetails(issue.getId(),issue);
        return  new ResponseEntity<>(list, HttpStatus.valueOf(list.getCode()));
    }

    @PutMapping(value = "/status/{id}/{status}")
    public  ResponseEntity<Result<List<Issue>>> updateIssueStatus(@PathVariable("id") int id,@PathVariable("status") String Status) throws  Exception{
        Result<List<Issue>> list=issueService.updateIssueStatus(id,Status);
        return  new ResponseEntity<>(list, HttpStatus.valueOf(list.getCode()));
    }
 }
