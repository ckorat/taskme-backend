package com.taskmebackend.issue.comment.model.mapper;

import com.taskmebackend.issue.comment.model.Comment;
import com.taskmebackend.issue.comment.model.CommentDetail;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CommentRowMapper implements RowMapper<CommentDetail> {
    @Override
    public CommentDetail mapRow(ResultSet rs, int i) throws SQLException {
        CommentDetail comment=new CommentDetail();
        comment.setId(rs.getInt("id"));
        comment.setIssueid(rs.getInt("issueid"));
        comment.setCommenter(rs.getInt("commenter"));
        comment.setComment(rs.getString("comment"));
        comment.setCommentdatetime(rs.getString("commentdatetime"));
        comment.setUsername(rs.getString("username"));
        return comment;
    }
}
