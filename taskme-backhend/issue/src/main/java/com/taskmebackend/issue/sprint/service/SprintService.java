package com.taskmebackend.issue.sprint.service;

import com.taskmebackend.issue.sprint.model.Sprint;
import com.taskmebackend.issue.sprint.repository.SprintRepository;
import com.taskmebackend.response.exception.ResultException;
import com.taskmebackend.response.response.Result;
import org.apache.catalina.LifecycleState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class SprintService {
    @Autowired
    SprintRepository sprintRepository;

    public Result<List<Sprint>> findAllSprint(){
        List<Sprint> list=sprintRepository.findAllSprint();
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new com.taskmebackend.response.response.Result<>(404, "No Sprints found"));
    }

    public Result<List<Sprint>> findActiveSprint(int projectid){
        List<Sprint> list=sprintRepository.findActiveSprint(projectid);
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new com.taskmebackend.response.response.Result<>(404, "No Sprints found"));
    }

    public Result<List<Sprint>> findInActiveSprint(){
        List<Sprint> list=sprintRepository.findInActiveSprint();
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new com.taskmebackend.response.response.Result<>(404, "No Sprints Active"));
    }

    public Result<List<Sprint>> findSprintById(int id){
        List<Sprint> list=sprintRepository.findSprintById(id);
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new com.taskmebackend.response.response.Result<>(404, "No Sprints Activeted"));
    }

    public Result<List<Sprint>> addSprint(Sprint sprint) throws Exception{
        int id=sprintRepository.addSprint(sprint);
        if(id > 0 ){
            return  new Result<>(201,"Sprint Created");
        }
        throw new ResultException(new Result<>(400, "Error!, please try again!", new ArrayList<>(Arrays
                .asList(new Result.TaskMeError(400, "unable to add the given Sprint")))));
    }

    public Result<List<Sprint>> updateSprint(int id,Sprint sprint) throws Exception{
        if(sprintRepository.updateSprint(id,sprint)){
            return  new Result<>(200,"Sprint Updated");
        }
        throw new ResultException(new Result<>(400, "Unable to update the given Sprint, please try again!",
                new ArrayList<>(Arrays.asList(new Result.TaskMeError(sprint.hashCode(),
                        "given SprintId('" + id + "') does not exists ")))));
    }
}
