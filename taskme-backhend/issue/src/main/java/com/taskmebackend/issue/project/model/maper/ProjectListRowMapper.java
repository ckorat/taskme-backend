package com.taskmebackend.issue.project.model.maper;

import com.taskmebackend.issue.project.model.ProjectList;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProjectListRowMapper implements RowMapper<ProjectList> {
    @Override
    public ProjectList mapRow(ResultSet resultSet, int i) throws SQLException {
        ProjectList projectList=new ProjectList();
        projectList.setId(resultSet.getInt("id"));
        projectList.setProjectName(resultSet.getString("projectname"));
        return projectList;
    }
}
