package com.taskmebackend.issue.project.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "db")
@Component
public class ProjectQueryService {

    @NotNull
    public DbQuery dbQueries;
    @Data
    @Component
    @AllArgsConstructor
    @NoArgsConstructor
    public static class DbQuery {
        public String findAllProject;
        public String findAllActiveProject;
        public String findAllInActiveProject;
        public String findProjectById;
        public String updateProject;
        public String insertProject;
        public String findAllSprint;
        public String findAllActiveSprint;
        public String findAllInActiveSprint;
        public String findSprintById;
        public String insertSprint;
        public String updateSprint;
        public String allIssue;
        public String getIssueById;
        public String getIssueByStatus;
        public String getIssueByAssigneeId;
        public String assignIssue;
        public String AddIssue;
        public String updateIssueDetails;
        public String updateIssueStatus;
        public String getIssueType;
        public String getIssueDetail;
        public String addUserProject;
        public String updateUserProject;
        public String removeUserProject;
        public String getUserByProjectId;
        public String getProjectListofUser;
        public String getMemberList;
        public String MeetingId;
        public String getProjectByUserId;
    }
}
