package com.taskmebackend.issue.task.model.maper;

import com.taskmebackend.issue.task.model.IssueType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class IssueTypeRowMapper implements RowMapper<IssueType> {
    @Override
    public IssueType mapRow(ResultSet rs, int i) throws SQLException {
        IssueType type=new IssueType();
        type.setId(rs.getInt("id"));
        type.setIssuename(rs.getString("issuename"));
        return type;
    }
}
