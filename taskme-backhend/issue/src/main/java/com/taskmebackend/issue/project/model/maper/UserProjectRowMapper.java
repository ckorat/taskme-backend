package com.taskmebackend.issue.project.model.maper;
import com.taskmebackend.issue.project.model.UserProject;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserProjectRowMapper  implements RowMapper<UserProject> {
    @Override
    public UserProject mapRow(ResultSet resultSet, int i) throws SQLException {
        UserProject userProject=new UserProject();
        userProject.setId(resultSet.getInt("id"));
        userProject.setUserId(resultSet.getInt("userid"));
        userProject.setProjectId(resultSet.getString("projectid"));
        return userProject;
    }
}
