package com.taskmebackend.issue.project.repository;

import com.taskmebackend.issue.project.model.*;

import java.util.List;

public interface ProjectRepository {
    List<Project> findAllProject();
    List<Project> getAllActiveProject(int id);
    List<Project> getAllInactiveProject(int id);
    List<Project> getProjectById(int id);
    List<ProjectList> getProjectListofUser(int id);
    List<MemberList> getMemberList(int id);
    int addProject(Project project) throws Exception;
    List<MeetingId> MeetingId();
    boolean UpdateProjectById(int id,Project project) throws Exception;
    int addUserProject(UserProject userProject) throws  Exception;
    boolean UpdateUserProjectById(int userid,String project) throws Exception;
    List<UserProject> getProjectByUserId(int userid);
    boolean removeUserProject(int userid) throws Exception;
}
