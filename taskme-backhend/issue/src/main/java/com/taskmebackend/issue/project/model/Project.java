package com.taskmebackend.issue.project.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Project {
    private int id;
    private int orgId;
    private String projectName;
    private  String projectDescription;
    private String createOn;
    private String completeOn;
}
