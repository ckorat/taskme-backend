package com.taskmebackend.issue.project.model.maper;

import com.taskmebackend.issue.project.model.MemberList;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MemberListRowMapper implements RowMapper<MemberList> {
    @Override
    public MemberList mapRow(ResultSet resultSet, int i) throws SQLException {
        MemberList memberList=new MemberList();
        memberList.setId(resultSet.getInt("id"));
        memberList.setUseremail(resultSet.getString("useremail"));
        return memberList;
    }
}
