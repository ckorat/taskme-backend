package com.taskmebackend.apigetway.authentication.service;

import com.taskmebackend.apigetway.authentication.model.ProfileModel;
import com.taskmebackend.apigetway.authentication.repository.ProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProfileService implements UserDetailsService {
    @Autowired
    private ProfileRepository profileRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final List<ProfileModel> users = profileRepository.getAllProfiles();
        for (ProfileModel appUser : users) {
            if (appUser.getUseremail().equals(username)) {
                List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                        .commaSeparatedStringToAuthorityList("ROLE_" + appUser.getRole().toUpperCase());
                return new User(appUser.getUseremail(), appUser.getPassword(), grantedAuthorities);
            }
        }
        throw new UsernameNotFoundException("Username: " + username + " not found");
    }
    public ProfileModel getProfileByUsername(String username) {
        return profileRepository.getProfileByUsername(username);
    }
}
