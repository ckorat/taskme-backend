package com.taskmebackend.apigetway.authentication.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProfileForLoginState {
    private int id;
    private String role;
    private String username;
    private int orgid;
}
