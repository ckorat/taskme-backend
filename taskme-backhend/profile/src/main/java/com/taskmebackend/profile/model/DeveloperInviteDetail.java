package com.taskmebackend.profile.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeveloperInviteDetail extends DeveloperInvite {
    private String projectname;
}
