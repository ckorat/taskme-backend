package com.taskmebackend.profile.resporitory;
import com.taskmebackend.profile.model.*;
import com.taskmebackend.profile.model.mapper.DeveloperInviteDetailRowMapper;
import com.taskmebackend.profile.model.mapper.OrganizationRowMapper;
import com.taskmebackend.profile.model.mapper.UserRowMapper;
import com.taskmebackend.profile.service.ProfileQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Repository(value = "profileRepository")
public class ProfileResporitoryImpl implements ProfileResporitory {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate jdbcNamed;

    @Autowired
    ProfileQueryService profileQueryService;

    @Override
    public boolean addProfile(Users user) {
        return jdbcTemplate.update(profileQueryService.getDbQueries().getInsertProfile(),user.getUsername(),user.getUseremail(),user.getUserpassword(),user.getUserrole(),user.getOrgid())>0;
    }

    @Override
    public boolean updateProfile(Users user, int id)  {
        return jdbcTemplate.update(profileQueryService.getDbQueries().getUpdateProfile(), user.getUsername(),user.getUseremail(),user.getUserpassword(),user.getUserrole(),user.getOrgid(),id) > 0;
    }

    @Override
    public List<Users> getAllProfile() {
        return jdbcTemplate.query(profileQueryService.getDbQueries().getGetAllProfile(),new UserRowMapper());
    }

    @Override
    public List<Users> getProfileById(int id)  {
        return jdbcTemplate.query(profileQueryService.getDbQueries().getGetProfileById(),new UserRowMapper(),id);
    }

    @Override
    public List<Users> getProfileByRole(String role)  {
        return jdbcTemplate.query(profileQueryService.getDbQueries().getGetProfileByRole(),new UserRowMapper(),role);
    }

    @Override
    public List<Users> getProfileIdByEmail(String email) throws Exception {
        return jdbcTemplate.query(profileQueryService.getDbQueries().getGetProfileIdByEmail(),new UserRowMapper(),email);
    }



    @Override
    public boolean addOrganization(Organization org)  {
        return jdbcTemplate.update(profileQueryService.getDbQueries().getInsertOrganization(),org.getOrgname())>0;
    }

    @Override
    public List<Organization> getAllOrganization()  {
        return jdbcTemplate.query(profileQueryService.getDbQueries().getGetAllOrganization(),new OrganizationRowMapper());
    }

    @Override
    public List<Organization> getOrganizationIdByName(String name)  {
        return jdbcTemplate.query(profileQueryService.getDbQueries().getGetOrganizationIdByName(),new OrganizationRowMapper(),name);
    }

    @Override
    public boolean addInviteDeveloper(DeveloperInvite developerInvite) throws Exception {
        return jdbcTemplate.update(profileQueryService.getDbQueries().getInsertInviteDeveloper(),developerInvite.getUseremail(),developerInvite.getProjectid(),developerInvite.getIsaccept())>0;
    }

    @Override
    public boolean removeInviteDeveloper(String useremail,int projectid) throws Exception {
        return jdbcTemplate.update(profileQueryService.dbQueries.getDeleteInviteDeveloper(),useremail,projectid)>0;
    }

    @Override
    public int updatePassword(UpdatePassword updatePassword) throws Exception {
        String selectSQL = "select id from users where id=:id and userpassword=:oldPassword";
        System.out.println(jdbcNamed.queryForList(selectSQL, new BeanPropertySqlParameterSource(updatePassword)).size());
        if (jdbcNamed.queryForList(selectSQL, new BeanPropertySqlParameterSource(updatePassword))
                .size() > 0) {
            String sql = "UPDATE `users` set `userpassword`=:newPassword  where `id`=:id";
            return jdbcNamed.update(sql, new BeanPropertySqlParameterSource(updatePassword));
        }
        return -1;
    }
    @Override
    public List<DeveloperInviteDetail> getInviteDeveloperByProjectId(int id) throws Exception {
        return jdbcTemplate.query(profileQueryService.getDbQueries().getGetInviteDeveloperByProjectId(),new DeveloperInviteDetailRowMapper(),id);
    }

    @Override
    public boolean updateInviteDeveloperByUserId(DeveloperInvite developerInvite) throws Exception {
        return jdbcTemplate.update(profileQueryService.getDbQueries().getUpdateInviteDeveloperByUserId(), developerInvite.getIsaccept(),developerInvite.getUseremail()) > 0;
    }


}
