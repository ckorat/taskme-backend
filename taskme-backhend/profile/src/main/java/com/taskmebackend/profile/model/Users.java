package com.taskmebackend.profile.model;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Users {
private int id;
private String username;
private String useremail;
private String userpassword;
private String userrole;
private String  orgid;
}
