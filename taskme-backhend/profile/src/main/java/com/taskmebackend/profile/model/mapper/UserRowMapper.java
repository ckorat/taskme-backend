package com.taskmebackend.profile.model.mapper;

import com.taskmebackend.profile.model.Users;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRowMapper implements RowMapper<Users> {
    @Override
    public Users mapRow(ResultSet rs, int rowNum) throws SQLException {
        Users user=new Users();
        user.setId(rs.getInt("id"));
        user.setUsername(rs.getString("username"));
        user.setUseremail(rs.getString("useremail"));
        user.setUserpassword(rs.getString("userpassword"));
        user.setUserrole(rs.getString("userrole"));
        user.setOrgid(rs.getString("orgid"));
        return user;
    }
}
