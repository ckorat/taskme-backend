package com.taskmebackend.profile.model.mapper;

import com.taskmebackend.profile.model.UpdatePassword;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UpdatePasswordRowMapper implements RowMapper<UpdatePassword> {
    @Override
    public UpdatePassword mapRow(ResultSet resultSet, int i) throws SQLException {
        UpdatePassword updatePassword=new UpdatePassword();
        updatePassword.setId(resultSet.getInt("id"));
        updatePassword.setOldPassword(resultSet.getString("userpassword"));
        updatePassword.setNewPassword(resultSet.getNString("userpassword"));
        return updatePassword;
    }
}
