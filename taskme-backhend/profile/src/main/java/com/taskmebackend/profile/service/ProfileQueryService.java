package com.taskmebackend.profile.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "db")
@Component
public class ProfileQueryService {

    @NotNull
    public DbQueries dbQueries;

    @Data
    @Component
    @AllArgsConstructor
    @NoArgsConstructor
    public static class DbQueries{
        private String insertProfile;
        private String updateProfile;
        private String getAllProfile;
        private String getProfileById;
        private String getProfileByRole;
        private String getProfileIdByEmail;
        private String insertOrganization;
        private String getAllOrganization;
        private String getOrganizationIdByName;
        private String getInviteDeveloperByProjectId;
        private String insertInviteDeveloper;
        private String updateInviteDeveloperByUserId;
        private String deleteInviteDeveloper;
    }
}

