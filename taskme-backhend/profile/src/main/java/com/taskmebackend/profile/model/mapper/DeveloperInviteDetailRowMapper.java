package com.taskmebackend.profile.model.mapper;

import com.taskmebackend.profile.model.DeveloperInviteDetail;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DeveloperInviteDetailRowMapper implements RowMapper<DeveloperInviteDetail> {
    @Override
    public DeveloperInviteDetail mapRow(ResultSet rs, int i) throws SQLException {
        DeveloperInviteDetail developerInviteDetail=new DeveloperInviteDetail();
        developerInviteDetail.setId(rs.getInt("id"));
        developerInviteDetail.setUseremail(rs.getString("useremail"));
        developerInviteDetail.setProjectid(rs.getInt("projectid"));
        developerInviteDetail.setProjectname(rs.getString("projectname"));
        developerInviteDetail.setIsaccept(rs.getInt("isaccept"));
        return developerInviteDetail;
    }
}
