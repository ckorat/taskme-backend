package com.taskmebackend.profile.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeveloperInvite {
    private int id;
    private String useremail;
    private int projectid;
    private int isaccept;
}
