package com.taskmebackend.profile.controller;

import com.taskmebackend.profile.model.*;
import com.taskmebackend.profile.service.ProfileService;
import com.taskmebackend.response.response.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/profiles",produces = "application/json")
public class ProfileController {

    @Autowired
    ProfileService profileService;

    @GetMapping(value = "/")
    public ResponseEntity<Result<List<Users>>> getAllProfiles() throws Exception {
        Result<List<Users>> result = profileService.getAllProfiles();
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Result<List<Users>>> getProfileById(@PathVariable(value = "id") int id) throws Exception {
        Result<List<Users>> result=profileService.getAllProfilesById(id);
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }
    @GetMapping(value = "/role/{role}")
    public ResponseEntity<Result<List<Users>>> getProfileByRole(@PathVariable(value = "role") String role) throws Exception {
        Result<List<Users>> result=profileService.getAllProfilesByRole(role);
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }
    @GetMapping(value = "/email/{email}")
    public ResponseEntity<Result<List<Users>>> getProfileByEmail(@PathVariable(value = "email") String email) throws Exception {
        Result<List<Users>> result=profileService.getAllProfileByEmail(email);
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }
    @GetMapping(value = "/organizations")
    public ResponseEntity<Result<List<Organization>>> getAllOrganizations() throws Exception {
        Result<List<Organization>> result=profileService.getAllOrganization();
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }
    @GetMapping(value = "/organization/{name}")
    public ResponseEntity<Result<List<Organization>>> getOrganizationIdByName(@PathVariable(value = "name") String name) throws Exception {
        Result<List<Organization>> result=profileService.getAllOrganizationIdByName(name);
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }
    @GetMapping(value = "/invite/{id}")
    public ResponseEntity<Result<List<DeveloperInviteDetail>>> getDeveloperByProjectId(@PathVariable(value = "id") int id) throws Exception {
        Result<List<DeveloperInviteDetail>> result=profileService.getInviteDeveloperByProjectId(id);
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @PostMapping(value = "/")
    public ResponseEntity<Result<Users>> addProfile(@RequestBody @Valid Users user) throws Exception {
        Result<Users> result = profileService.addProfile(user);
        return  new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }
    @PostMapping(value = "/organization")
    public ResponseEntity<Result<Organization>> addOrganization(@RequestBody @Valid Organization org) throws Exception {
        Result<Organization> result = profileService.addOrganization(org);
        return  new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }
    @PostMapping(value = "/invite")
    public ResponseEntity<Result<DeveloperInvite>> addInviteDeveloper(@RequestBody @Valid DeveloperInvite developerInvite ) throws Exception {
        Result<DeveloperInvite> result = profileService.addInviteDeveloper(developerInvite);
        return  new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }
    @PutMapping(value = "/")
    public ResponseEntity<Result<Users>> updateProfile( @RequestBody @Valid Users user) throws Exception {
        Result<Users> result = profileService.updateProfile(user,user.getId());
        return  new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }
    @PutMapping(value = "/invite/")
    public ResponseEntity<Result<DeveloperInvite>> updateInviteDeveloperByUserId( @RequestBody @Valid DeveloperInvite developerInvite) throws Exception {
        Result<DeveloperInvite> result = profileService.updateInviteDeveloperByUserId(developerInvite);
        return  new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }
    @DeleteMapping(value = "/invite/")
    public ResponseEntity<Result<DeveloperInvite>>  removeInviteDeveloper(@RequestParam String useremail, @RequestParam int projectid) throws Exception{
        Result<DeveloperInvite> result = profileService.removeInviteDeveloper(useremail, projectid);
        return  new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }
    @PutMapping("/password")
    public ResponseEntity<Result<String>> updatePassword(
            @RequestBody(required = true) @Valid UpdatePassword updatePassword) throws Exception {
        System.out.println(updatePassword);
        Result<String> result = profileService.updatePassword(updatePassword);

        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }
}
