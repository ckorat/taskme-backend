package com.taskmebackend.profile.resporitory;

import com.taskmebackend.profile.model.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProfileResporitory {
    boolean addProfile(Users user) throws Exception;
    boolean updateProfile(Users user,int id) throws  Exception;
    List<Users> getAllProfile() throws Exception;
    List<Users> getProfileById(int id) throws Exception;
    List<Users> getProfileByRole(String role) throws  Exception;
    List<Users> getProfileIdByEmail(String email) throws  Exception;
    boolean addOrganization(Organization org) throws Exception;
    List<Organization> getAllOrganization() throws Exception;
    List<Organization> getOrganizationIdByName(String name) throws Exception;
    boolean addInviteDeveloper(DeveloperInvite developerInvite) throws Exception;
    int updatePassword(UpdatePassword updatePassword) throws Exception;
    boolean removeInviteDeveloper(String useremail,int projectid) throws Exception;
    List<DeveloperInviteDetail>getInviteDeveloperByProjectId(int id) throws Exception;
    boolean updateInviteDeveloperByUserId(DeveloperInvite developerInvite) throws Exception;
}
