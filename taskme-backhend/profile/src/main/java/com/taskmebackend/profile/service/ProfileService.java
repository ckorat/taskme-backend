package com.taskmebackend.profile.service;

import com.taskmebackend.profile.model.*;
import com.taskmebackend.profile.resporitory.ProfileResporitory;

import com.taskmebackend.response.exception.ResultException;
import com.taskmebackend.response.response.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ProfileService {
    @Autowired
    @Qualifier("profileRepository")
    ProfileResporitory profileResporitory;

    public Result<List<Users>> getAllProfiles() throws Exception {
        List<Users> list = profileResporitory.getAllProfile();
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new Result<>(404, "No Profile found"));
    }

    public Result<List<Users>> getAllProfilesById(int id) throws Exception {
        List<Users> list = profileResporitory.getProfileById(id);
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new Result<>(404, "No Profile found"));
    }
    public Result<List<Users>> getAllProfilesByRole(String role) throws Exception {
        List<Users> list = profileResporitory.getProfileByRole(role);
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new Result<>(404, "No Profile found"));
    }
    public Result<List<Users>> getAllProfileByEmail(String email) throws Exception {
        List<Users> list = profileResporitory.getProfileIdByEmail(email);
//        if (!list.isEmpty())
            return new Result<>(200,list);
//        throw new ResultException(new Result<>(404, "No Profile found"));
    }
    public Result<List<Organization>> getAllOrganization() throws Exception {
        List<Organization> list = profileResporitory.getAllOrganization();
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new Result<>(404, "No Organization found"));
    }
    public Result<List<Organization>> getAllOrganizationIdByName(String name) throws Exception {
        List<Organization> list = profileResporitory.getOrganizationIdByName(name);
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new Result<>(404, "No Organization found"));
    }
    public Result<Users> addProfile(Users user) throws Exception{
        if(profileResporitory.addProfile(user)){
            return  new Result<>(201,"Profile Created");
        }
        throw new ResultException(new Result<>(400, "Error!, please try again!", new ArrayList<>(Arrays
                .asList(new Result.TaskMeError(400, "unable to add the given profile")))));
    }

    public Result<Users> updateProfile(Users user,int id) throws Exception{
        if(profileResporitory.updateProfile(user,id)){
            return  new Result<>(200,"Profile Updated");
        }
        throw new ResultException(new Result<>(400, "Error!, please try again!", new ArrayList<>(Arrays
                .asList(new Result.TaskMeError(400, "unable to update the given organization")))));
    }

    public Result<Organization> addOrganization(Organization org) throws Exception{
        if(profileResporitory.addOrganization(org)){
            return  new Result<>(200,"Profile Updated");
        }
        throw new ResultException(new Result<>(400, "Error!, please try again!", new ArrayList<>(Arrays
                .asList(new Result.TaskMeError(400, "unable to update the given organization")))));
    }

    public Result<List<DeveloperInviteDetail>> getInviteDeveloperByProjectId(int id) throws Exception {
        List<DeveloperInviteDetail> list = profileResporitory.getInviteDeveloperByProjectId(id);
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new Result<>(404, "No invite developer found"));
    }

    public Result<DeveloperInvite> addInviteDeveloper(DeveloperInvite developerInvite) throws Exception{
        if(profileResporitory.addInviteDeveloper(developerInvite)){
            return  new Result<>(201,"InviteDeveloper Added");
        }
        throw new ResultException(new Result<>(400, "Error!, please try again!", new ArrayList<>(Arrays
                .asList(new Result.TaskMeError(400, "unable to added the given invite developer")))));
    }
    public Result<DeveloperInvite> removeInviteDeveloper(String useremail,int projectid) throws Exception{
        if(profileResporitory.removeInviteDeveloper(useremail,projectid)){
            return  new Result<>(200,"InviteDeveloper Deleted");
        }
        throw new ResultException(new Result<>(400, "Error!, please try again!", new ArrayList<>(Arrays
                .asList(new Result.TaskMeError(400, "unable to delete the given invite developer")))));
    }

    public Result<String> updatePassword(UpdatePassword updatePassword) throws Exception {
        int result = profileResporitory.updatePassword(updatePassword);
        if (result > 0) {
            return new Result<>(200, "password has been succefully updated");
        } else if (result == -1) {
            throw new ResultException(new Result<>(400, "old password does not match!"));
        } else {
            throw new ResultException(new Result<>(400, "Error!, please try again!", new ArrayList<>(Arrays
                    .asList(new Result.TaskMeError(400, "unable to Update the given Password")))));
        }
    }
        public Result<DeveloperInvite> updateInviteDeveloperByUserId (DeveloperInvite developerInvite) throws Exception
        {
            if (profileResporitory.updateInviteDeveloperByUserId(developerInvite)) {
                return new Result<>(200, "Developer Updated");
            }
            throw new ResultException(new Result<>(400, "Error!, please try again!", new ArrayList<>(Arrays
                    .asList(new Result.TaskMeError(400, "unable to update the given developer Invite")))));
        }

}
