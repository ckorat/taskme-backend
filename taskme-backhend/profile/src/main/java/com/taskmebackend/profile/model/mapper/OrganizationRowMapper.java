package com.taskmebackend.profile.model.mapper;

import com.taskmebackend.profile.model.Organization;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OrganizationRowMapper implements RowMapper<Organization> {
    @Override
    public Organization mapRow(ResultSet rs, int rowNum) throws SQLException {
        Organization org=new Organization();
        org.setId(rs.getInt("id"));
        org.setOrgname(rs.getString("orgname"));
        return org;
    }
}
