package com.taskmebackend.email.service;

import com.taskmebackend.email.model.ContactUs;
import com.taskmebackend.email.model.ForgetPassword;
import com.taskmebackend.response.exception.ResultException;
import com.taskmebackend.response.response.Result;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

@Service
public class ForgetPasswordService {
    public Result<ForgetPassword> resetForgetPassword(ForgetPassword forgetPassword) throws Exception, AddressException, MessagingException {
        try {
            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");

            Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication("taskmeindia@gmail.com", "TaskMe2020");
                }
            });

            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress("taskmeindia@gmail.com", false));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(forgetPassword.getEmailaddress()));
            msg.setSubject("TaskMe Password Reset");
            msg.setContent("<html>\n" +
                    "\t<body>\n" +
                    "\tHello TaskMe User,\n" +
                    "\t<br>\n" +
                    "\tEmail: <b>" + forgetPassword.getEmailaddress() + "</b>\n" +
                    "\t<p>\n" +
                    "\t\t Password :" + forgetPassword.getPassword() + "\n" +
                    "\t<p>\n" +
                    "\t</body>\n" +
                    "</html>", "text/html");
            msg.setSentDate(new Date());
            Transport.send(msg);
            return new Result<>(200, "Email sent successfully !!");
        }catch (Exception ex){
            throw new ResultException(new Result<>(400, "Error!, please try again!", new ArrayList<>(Arrays
                    .asList(new Result.TaskMeError(400, "unable to sent email.")))));
        }
    }
}
