package com.taskmebackend.email.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Invitation {
    private String recipient;
    private String sender;
    private String project;
    private String url;
}
