package com.taskmebackend.email.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContactUs {
    private String Name;
    private String emailaddress;
    private String message;
}
