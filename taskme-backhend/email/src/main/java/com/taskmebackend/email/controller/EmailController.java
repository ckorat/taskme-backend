package com.taskmebackend.email.controller;

import com.taskmebackend.email.model.ContactUs;
import com.taskmebackend.email.model.ForgetPassword;
import com.taskmebackend.email.model.Invitation;
import com.taskmebackend.email.service.ContactUsService;
import com.taskmebackend.email.service.ForgetPasswordService;
import com.taskmebackend.email.service.InvitationService;
import com.taskmebackend.response.response.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/email",produces = "application/json")
public class EmailController {
    @Autowired
    InvitationService invitationService;

    @Autowired
    ContactUsService contactUsService;

    @Autowired
    ForgetPasswordService forgetPasswordService;

    @PostMapping(value = "/invitation")
    public ResponseEntity<Result<Invitation>> sendInvitation(@RequestBody @Valid Invitation invitation) throws Exception {
        Result<Invitation> result = invitationService.sendInvitation(invitation);
        return  new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @PostMapping(value = "/contactus")
    public ResponseEntity<Result<ContactUs>> sendContactUs(@RequestBody @Valid ContactUs contactUs) throws Exception {
        Result<ContactUs> result =  contactUsService.sendContactUs(contactUs);
        return  new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }
    @PostMapping(value = "/reset")
    public ResponseEntity<Result<ForgetPassword>> sendContactUs(@RequestBody @Valid ForgetPassword forgetPassword) throws Exception {
        Result<ForgetPassword> result =  forgetPasswordService.resetForgetPassword(forgetPassword);
        return  new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }
}
