package com.taskmebackend.email.service;

import com.taskmebackend.email.model.Invitation;
import com.taskmebackend.response.exception.ResultException;
import com.taskmebackend.response.response.Result;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

@Service
public class InvitationService {
    public Result<Invitation> sendInvitation(Invitation invitation) throws Exception, AddressException, MessagingException {
        try {
            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");

            Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication("taskmeindia@gmail.com", "TaskMe2020");
                }
            });

            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress("taskmeindia@gmail.com", false));

            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(invitation.getRecipient()));
            msg.setSubject(invitation.getSender() + " has invited you to join a " + invitation.getProject() + " project.");
            msg.setContent("<html>\n" +
                    "\t<body>\n" +
                    "\tHello<b> Developer<b>,\n" +
                    "\t<br>\n" +
                    "\t<p>\n" +
                    "\t\t" + invitation.getSender() + " has invited you to join the project.Join now to start collaborating!\n" +
                    "\t<p>\n" +
                    "\t<br>\n" +
                    "\t " + invitation.getUrl() +
                    "\t</body>\n" +
                    "</html>", "text/html");
            msg.setSentDate(new Date());
            Transport.send(msg);
            return new Result<>(200, "Email sent sucessfully");
        }catch (Exception ex){
            throw new ResultException(new Result<>(400, "Error!, please try again!", new ArrayList<>(Arrays
                    .asList(new Result.TaskMeError(400, "unable to sent invitation.")))));
        }
    }
}
