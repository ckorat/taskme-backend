package com.taskmebackend.common.meeting.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MeetingInvite {
    private int id;
    private int userid;
    private int meetingid;
}
