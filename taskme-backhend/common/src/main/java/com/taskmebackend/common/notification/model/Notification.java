package com.taskmebackend.common.notification.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Notification {
    private int id;
    private int recipientid;
    private String issueid;
    private String message;
    private String notificationdatetime;
    private String type;
    private int isread;
}
