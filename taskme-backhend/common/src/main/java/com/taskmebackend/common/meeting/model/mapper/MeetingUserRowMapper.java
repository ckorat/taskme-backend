package com.taskmebackend.common.meeting.model.mapper;

import com.taskmebackend.common.meeting.model.MeetingUser;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MeetingUserRowMapper implements RowMapper<MeetingUser> {
    @Override
    public MeetingUser mapRow(ResultSet rs, int rowNum) throws SQLException {
        MeetingUser meetingUser=new MeetingUser();
        meetingUser.setId(rs.getInt("id"));
        meetingUser.setUserid(rs.getInt("userid"));
        meetingUser.setProjectid(rs.getInt("projectid"));
        meetingUser.setCreatorid(rs.getInt("creatorid"));
        meetingUser.setMeetingtitle(rs.getString("meetingtitle"));
        meetingUser.setMeetingdescription(rs.getString("meetingdescription"));
        meetingUser.setMeetingdate(rs.getString("meetingdate"));
        meetingUser.setMeetingtime(rs.getString("meetingtime"));
        meetingUser.setTypeid(rs.getInt("typeid"));
        return meetingUser;
    }
}
