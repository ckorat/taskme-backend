package com.taskmebackend.common.meeting.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "db")
@Component
public class MeetingQueryService {
    @NotNull
    public DbQueries dbQueries;

    @Data
    @Component
    @AllArgsConstructor
    @NoArgsConstructor
    public static class DbQueries{
        private String getMeetingByUserID;
        private String getMeetingByID;
        private String addMeeting;
        private String addInviteMeeting;
    }
}
