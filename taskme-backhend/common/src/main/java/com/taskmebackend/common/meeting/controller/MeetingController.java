package com.taskmebackend.common.meeting.controller;

import com.taskmebackend.common.meeting.model.Meeting;
import com.taskmebackend.common.meeting.model.MeetingDetails;
import com.taskmebackend.common.meeting.model.MeetingInvite;
import com.taskmebackend.common.meeting.model.MeetingUser;
import com.taskmebackend.common.meeting.service.MeetingService;
import com.taskmebackend.response.response.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/meetings")
public class MeetingController {
    @Autowired
    MeetingService meetingService;


    @GetMapping(value = "/{id}")
    public ResponseEntity<Result<List<MeetingDetails>>> getMeetingById(@PathVariable(value = "id") int id) throws Exception {
        Result<List<MeetingDetails>> result=meetingService.getMeetingByID(id);
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }
    @GetMapping(value = "/user/{id}")
    public ResponseEntity<Result<List<MeetingUser>>> getMeetingByUserId(@PathVariable(value = "id") int id) throws Exception {
        Result<List<MeetingUser>> result=meetingService.getMeetingByUserID(id);
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @PostMapping(value = "/")
    public ResponseEntity<Result<Meeting>> addMeeting(@RequestBody @Valid Meeting meeting) throws Exception {
        Result<Meeting> result=meetingService.addMeeting(meeting);
        return  new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }

    @PostMapping(value = "/invite")
    public ResponseEntity<Result<MeetingInvite>> addMeetingInvite(@RequestBody @Valid MeetingInvite meetingInvite) throws Exception {
        Result<MeetingInvite> result= meetingService.addMeetingInvite(meetingInvite);
        return  new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }
}
