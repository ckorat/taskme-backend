package com.taskmebackend.common.meeting.repository;

import com.taskmebackend.common.meeting.model.Meeting;
import com.taskmebackend.common.meeting.model.MeetingDetails;
import com.taskmebackend.common.meeting.model.MeetingInvite;
import com.taskmebackend.common.meeting.model.MeetingUser;
import com.taskmebackend.common.meeting.model.mapper.MeetingDetailRowMapper;
import com.taskmebackend.common.meeting.model.mapper.MeetingUserRowMapper;
import com.taskmebackend.common.meeting.service.MeetingQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(value = "meetingRepository")
public class MeetingRepositoryImpl implements MeetingRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    MeetingQueryService meetingQueryService;

    @Override
    public boolean addMeeting(Meeting meeting) throws Exception {
        return jdbcTemplate.update(meetingQueryService.getDbQueries().getAddMeeting(),meeting.getProjectid(),meeting.getCreatorid(),meeting.getMeetingtitle(),meeting.getMeetingdescription(),meeting.getMeetingdate(),meeting.getMeetingtime(),meeting.getTypeid())>0;
    }

    @Override
    public boolean addMeetingInvite(MeetingInvite meetingInvite) throws Exception {
        return jdbcTemplate.update(meetingQueryService.getDbQueries().getAddInviteMeeting(),meetingInvite.getUserid(),meetingInvite.getMeetingid())>0;
    }

    @Override
    public List<MeetingUser> getMeetingByUserID(int id) throws Exception {
        return jdbcTemplate.query(meetingQueryService.getDbQueries().getGetMeetingByUserID(),new MeetingUserRowMapper(),id);
    }

    @Override
    public List<MeetingDetails> getMeetingByID(int id) throws Exception {
        return jdbcTemplate.query(meetingQueryService.getDbQueries().getGetMeetingByID(),new MeetingDetailRowMapper(),id);
    }
}
