package com.taskmebackend.common.meeting.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MeetingDetails extends Meeting {
    private String projectname;
    private String username;
    private String typename;
}
