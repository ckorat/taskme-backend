package com.taskmebackend.common.notification.repository;

import com.taskmebackend.common.notification.model.Notification;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationRepository  {
    boolean addNotification(Notification notification) throws Exception;
    List<Notification> getNotificationByUserId(int id) throws Exception;
    boolean removeNotification(int id) throws Exception;
}
