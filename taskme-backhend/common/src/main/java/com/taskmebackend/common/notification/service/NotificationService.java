package com.taskmebackend.common.notification.service;

import com.taskmebackend.common.meeting.model.Meeting;
import com.taskmebackend.common.meeting.model.MeetingDetails;
import com.taskmebackend.common.notification.model.Notification;
import com.taskmebackend.common.notification.repository.NotificationRepository;
import com.taskmebackend.response.exception.ResultException;
import com.taskmebackend.response.response.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class NotificationService  {
    @Autowired
    @Qualifier("notificationRepository")
    NotificationRepository notificationRepository;

    public Result<List<Notification>> getNotificationByUserId(int id) throws Exception {
        List<Notification> list = notificationRepository.getNotificationByUserId(id);
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new Result<>(404, "No Notification found"));
    }
    public Result<Notification> addNotification(Notification notification) throws Exception{
        if(notificationRepository.addNotification(notification)){
            return  new Result<>(201,"Notification Created");
        }
        throw new ResultException(new Result<>(400, "Error!, please try again!", new ArrayList<>(Arrays
                .asList(new Result.TaskMeError(400, "unable to add the given notification")))));
    }
    public Result<Notification> removeNotification(int id) throws Exception{
        if(notificationRepository.removeNotification(id)){
            return  new Result<>(200,"Notification Removed");
        }
        throw new ResultException(new Result<>(400, "Error!, please try again!", new ArrayList<>(Arrays
                .asList(new Result.TaskMeError(400, "unable to remove the given notification")))));
    }
}
