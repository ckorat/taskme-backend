package com.taskmebackend.common.notification.model.mapper;

import com.taskmebackend.common.notification.model.Notification;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class NotificationRowMapper implements RowMapper<Notification> {
    @Override
    public Notification mapRow(ResultSet rs, int rowNum) throws SQLException {
        Notification notification=new Notification();
        notification.setId(rs.getInt("id"));
        notification.setRecipientid(rs.getInt("recipientid"));
        notification.setIssueid(rs.getString("issueid"));
        notification.setMessage(rs.getString("message"));
        notification.setNotificationdatetime(rs.getString("notificationdatetime"));
        notification.setType(rs.getString("type"));
        notification.setIsread(rs.getInt("isread"));
        return notification;
    }
}
