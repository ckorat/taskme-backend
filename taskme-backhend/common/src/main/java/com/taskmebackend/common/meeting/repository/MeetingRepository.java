package com.taskmebackend.common.meeting.repository;

import com.taskmebackend.common.meeting.model.Meeting;
import com.taskmebackend.common.meeting.model.MeetingDetails;
import com.taskmebackend.common.meeting.model.MeetingInvite;
import com.taskmebackend.common.meeting.model.MeetingUser;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MeetingRepository {
    boolean addMeeting(Meeting meeting) throws  Exception;
    boolean addMeetingInvite(MeetingInvite meetingInvite) throws  Exception;
    List<MeetingUser> getMeetingByUserID(int id) throws Exception;
    List<MeetingDetails> getMeetingByID(int id) throws Exception;
}
