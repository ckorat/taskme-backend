package com.taskmebackend.common.meeting.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Meeting {
    private int id;
    private int projectid;
    private int creatorid;
    private String meetingtitle;
    private String meetingdescription;
    private String meetingdate;
    private String meetingtime;
    private int typeid;
}
