package com.taskmebackend.common.meeting.service;

import com.taskmebackend.common.meeting.model.Meeting;
import com.taskmebackend.common.meeting.model.MeetingDetails;
import com.taskmebackend.common.meeting.model.MeetingInvite;
import com.taskmebackend.common.meeting.model.MeetingUser;
import com.taskmebackend.common.meeting.repository.MeetingRepository;
import com.taskmebackend.response.exception.ResultException;
import com.taskmebackend.response.response.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class MeetingService {
    @Autowired
    @Qualifier("meetingRepository")
    MeetingRepository meetingRepository;

    public Result<List<MeetingUser>> getMeetingByUserID(int id) throws Exception {
        List<MeetingUser> list = meetingRepository.getMeetingByUserID(id);
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new Result<>(404, "No Meeting found"));
    }
    public Result<List<MeetingDetails>> getMeetingByID(int id) throws Exception {
        List<MeetingDetails> list = meetingRepository.getMeetingByID(id);
        if (!list.isEmpty())
            return new Result<>(200,list);
        throw new ResultException(new Result<>(404, "No Meeting found"));
    }
    public Result<Meeting> addMeeting(Meeting meeting) throws Exception{
        if(meetingRepository.addMeeting(meeting)){
            return  new Result<>(201,"Meeting Created");
        }
        throw new ResultException(new Result<>(400, "Error!, please try again!", new ArrayList<>(Arrays
                .asList(new Result.TaskMeError(400, "unable to add the given meeting")))));
    }
    public Result<MeetingInvite> addMeetingInvite(MeetingInvite meetingInvite) throws Exception{
        if(meetingRepository.addMeetingInvite(meetingInvite)){
            return  new Result<>(201,"Meeting Invitation Created");
        }
        throw new ResultException(new Result<>(400, "Error!, please try again!", new ArrayList<>(Arrays
                .asList(new Result.TaskMeError(400, "unable to add the given meeting invitation")))));
    }
}
