package com.taskmebackend.common.notification.controller;

import com.taskmebackend.common.meeting.model.Meeting;
import com.taskmebackend.common.meeting.model.MeetingUser;
import com.taskmebackend.common.notification.model.Notification;
import com.taskmebackend.common.notification.service.NotificationService;
import com.taskmebackend.response.response.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/notifications",produces = "application/json")
public class NotificationController {
    @Autowired
    NotificationService notificationService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<Result<List<Notification>>> getMeetingByUserId(@PathVariable(value = "id") int id) throws Exception {
        Result<List<Notification>> result=notificationService.getNotificationByUserId(id);
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @PostMapping(value = "/")
    public ResponseEntity<Result<Notification>> addNotification(@RequestBody @Valid Notification notification) throws Exception {
        Result<Notification> result=notificationService.addNotification(notification);
        return  new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Result<Notification>> removeNotification(@PathVariable(value = "id") int id) throws Exception {
        Result<Notification> result=notificationService.removeNotification(id);
        System.out.println(result);
        return  new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }
}
