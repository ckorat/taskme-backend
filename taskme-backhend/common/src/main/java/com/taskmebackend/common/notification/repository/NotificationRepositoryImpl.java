package com.taskmebackend.common.notification.repository;

import com.taskmebackend.common.notification.model.Notification;
import com.taskmebackend.common.notification.model.mapper.NotificationRowMapper;
import com.taskmebackend.common.notification.service.NotificationQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(value = "notificationRepository")
public class NotificationRepositoryImpl implements NotificationRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NotificationQueryService notificationQueryService;

    @Override
    public boolean addNotification(Notification notification) throws Exception {
        return jdbcTemplate.update(notificationQueryService.getDbQueries().getAddNotification(),notification.getRecipientid(),notification.getIssueid(),notification.getMessage(),notification.getNotificationdatetime(),notification.getType(),notification.getIsread())>0;
    }

    @Override
    public List<Notification> getNotificationByUserId(int id) throws Exception {
            return jdbcTemplate.query(notificationQueryService.getDbQueries().getGetNotificationByUserId(),new NotificationRowMapper(),id);
    }

    @Override
    public boolean removeNotification(int id) throws Exception {
        return jdbcTemplate.update(notificationQueryService.getDbQueries().getRemoveNotification(),id)>0;
    }
}
