package com.taskmebackend.common.meeting.model.mapper;

import com.taskmebackend.common.meeting.model.MeetingDetails;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MeetingDetailRowMapper implements RowMapper<MeetingDetails> {
    @Override
    public MeetingDetails mapRow(ResultSet rs, int i) throws SQLException {
        MeetingDetails meetingDetails=new MeetingDetails();
        meetingDetails.setId(rs.getInt("id"));
        meetingDetails.setProjectname(rs.getString("projectname"));
        meetingDetails.setUsername(rs.getString("username"));
        meetingDetails.setMeetingtitle(rs.getString("meetingtitle"));
        meetingDetails.setMeetingdescription(rs.getString("meetingdescription"));
        meetingDetails.setMeetingdate(rs.getString("meetingdate"));
        meetingDetails.setMeetingtime(rs.getString("meetingtime"));
        meetingDetails.setTypename(rs.getString("typename"));
        return meetingDetails;
    }
}
